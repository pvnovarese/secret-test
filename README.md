# secret-test

Demo image to trigger Anchore Enterprise secret scans for embedded credentials.

See "secret-scan-policy.json" for an example policy.
